# Abstract


For my project, I will be creating a parallelized sudoku solver. Although sudoku puzzles do not
require a computer to solve, the scope of my project is comparing the speed of sequential programs to
parallel programs. I would also like to explore the topic of scalability of a process as the sample space
becomes much larger. In addition to this, I would also like to look for ways to optimize the decision
process of the program, based on how completed each row/column/box is. There are many ways to
complete a fast and parallel sudoku solver, but I want to experiment with a combination of each method
to increase my exposure to new software. I believe a successful project would be creating a scalable
code, where the same number of cores are still used regardless of cores and optimizing the selection
process for running functions.



# How to use
- Open Develpment Session in the HPCC
- type the commands
    - git clone 'https://gitlab.msu.edu/ramseybe/sudokusolver_ramsey/-/blob/master/simple_sudoku.py'
    - cd sudokusolver_ramsey/
    - mpiexec -n 9 python3 simple_sudoku.py
-The Output of the Code is where my program was able to insert the number at the given vertices

