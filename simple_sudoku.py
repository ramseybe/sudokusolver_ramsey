
def fill_grid(table,number):
    blocks = []
    for i in range(0, 9, 3):
        for j in range(0, 9, 3):
            blocks.append(table[i][j:j+3] + table[i+1][j:j+3] + table[i+2][j:j+3])
    filled_blocks=[] 
    for i in range(len(blocks)):
        if blocks[i].count(number) ==0:
            filled_blocks.append(blocks[i])

        else:
            filled_blocks.append([number for i in range(len(table))])
    new_blocks = []
    for i in range(0, 9, 3):
        for j in range(0, 9, 3):
            new_blocks.append(filled_blocks[i][j:j+3] + filled_blocks[i+1][j:j+3] + filled_blocks[i+2][j:j+3]) 
    return new_blocks
def fill_column(table,number):
    cols=[]
    table=list(map(list, zip(*table)))
    for i in range(len(table)):
        if table[i].count(number) == 0:
            cols.append(table[i])

        else:
            cols.append([number for i in range(len(table))])
    cols=list(map(list, zip(*cols)))
    return cols

def fill_rows(table,number):
    rows=[]
    for i in range(len(table)):
        if table[i].count(number) ==0:
            rows.append(table[i])

        else:
            rows.append([number for i in range(len(table))])
    return rows
def fill_number_new(table,number):
    rows=fill_rows(table,number)
    cols=fill_column(table,number)
    grids=fill_grid(table,number)

    fillednum=[]
    for r,c,g,s in zip(rows,cols,grids,table):
        temp_row=[]
        for i in range(len(r)):

            if r[i]==number or c[i]==number or g[i] == number:
                temp_row.append(number)
            elif s[i] != 0:

                temp_row.append(number)
            else:
                temp_row.append(s[i])

        fillednum.append(temp_row)
    return fillednum
def check_row(table):
    verts=[]
    for x,i in enumerate(table):
        if i.count(0)==1:
            verts.append((x,i.index(0)))
    return verts
def check_column(table):
    verts=[]
    table=list(map(list, zip(*sudoku)))
    for x,i in enumerate(table):
        if i.count(0)==1:
            verts.append((i.index(0),x))
    return verts

def box_solver(table,size):
    for x,y in zip(range(size),range(size)):
        startRow = row - row % 3
        startCol = col - col % 3
        for i in range(3):
            for j in range(3):
                if grid[i + startRow][j + startCol] == num:
                    return False
    return True
                    #return True
        #return True

def applied(sudoku,j):
    update=True
    while update==True:
        update=False
        temp=fill_number_new(sudoku,j)
        verts=check_row(temp)+check_column(temp)
        return verts
        for v in verts:
            sudoku[v[0]][v[1]]=j
            update=True

sudoku = [[2, 5, 0, 0, 3, 0, 9, 0, 1],
        [0, 1, 0, 0, 0, 4, 0, 0, 0],
    [4, 0, 7, 0, 0, 0, 2, 0, 8],
    [0, 0, 5, 2, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 9, 8, 1, 0, 0],
    [0, 4, 0, 0, 0, 3, 0, 0, 0],
    [0, 0, 0, 3, 6, 0, 0, 7, 2],
    [0, 7, 0, 0, 0, 0, 0, 0, 3],
    [9, 0, 3, 0, 0, 0, 6, 0, 4]]
grid=sudoku

    
from mpi4py import MPI
import numpy

comm = MPI.COMM_WORLD
rank = comm.Get_rank()



# automatic MPI datatype discovery
if rank == 0:

    grid1=applied(grid,rank+1)
    print(grid1,rank+1)

elif rank == 1:
    grid2=applied(grid,rank+1)
    print(grid2,rank+1)
elif rank == 2:
    grid3=applied(grid,rank+1)
    print(grid3,rank+1)
elif rank == 3:
    grid4=applied(grid,rank+1)
    print(grid4,rank+1)
elif rank == 4:
    grid4=applied(grid,rank+1)
    print(grid4,rank+1)
elif rank == 5:
    grid5=applied(grid,rank+1)
    print(grid5,rank+1)
elif rank == 6:
    grid6=applied(grid,rank+1)
    print(grid6,rank+1)
    
elif rank == 7:
    grid4=applied(grid,rank+1)
    print(grid4,rank+1)
    
elif rank == 8:
    grid4=applied(grid,rank+1)
    print(grid4,rank+1)
